#ifndef GRAPH_H_INCLUDED
#define GRAPH_H_INCLUDED

#include <iostream>
#include <list>
#include <vector>
#include <string>
#include <time.h>
#include <stdlib.h>


using namespace std;

uint64_t rdtsc()
{
    uint32_t a,b;
    __asm__ __volatile__("rdtsc":"=a"(a),"=d"(b));
    return ((uint64_t)a|((uint64_t)b)<<32);
}

template<class G>
class CNode
{
    typedef typename G::E E;
    typedef typename G::Edge Edge;
    typedef typename G::lista lst;
public:
    vector<Edge*> m_edges;
    E m_data;
    CNode(int x){
        m_data=x;
    }
    ~CNode();
};

template<class G>
class CEdge
{
    typedef typename G::N N;
    typedef typename G::Node Node;
public:
    int m_data;
    Node* m_nodes[2];
    bool m_dir; //0 = bidirecional  1= de origen a destino
    CEdge(Node* a, Node* b, int x, bool dir)
    {
        m_data=x;
        m_nodes[0]=a;
        m_nodes[1]=b;
        m_dir=dir;
    }
    ~CEdge(){

    }
};

template<class A,class B>
class CGraph
{
public:
    typedef CGraph<A,B> self;
    typedef CNode<self> Node;
    typedef CEdge<self> Edge;
    typedef list<Edge*> lista;
    typedef A N;
    typedef B E;
    CGraph(){};
    ~CGraph(){};
    vector<Node*> m_nodes;
    bool insertar_Node(N x){
        Node* tmp = new Node(x);
        m_nodes.push_back(tmp);
    }
    bool insertar_arista(Node* a, Node* b, E x, bool dir)
    {
        Node* aa;
        Node* bb;
        Edge* ar;

        if (find_edge_x(a,b))
            return 1;
        else
        {
            if (dir)
            {
                ar=new Edge(a,b,x,dir);
                a->m_edges.push_back(ar);
            }
            else
            {
                ar=new Edge(a,b,x,dir);
                a->m_edges.push_back(ar);
                ar=new Edge(b,a,x,dir);
                b->m_edges.push_back(ar);

            }
        }
    }
    bool insertar_arista_x(N a, N b, E x, bool dir)
    {
        Node* aa;
        Node* bb;
        Edge* ar;
        int i,j;
        search(a,i);
        search(b,j);
        aa=m_nodes[i],bb=m_nodes[j];
        if (find_edge_x(aa,bb))
            return 1;
        else
        {
            if (dir)
            {
//                ar=new Edge(a,b,x,dir);
//                a->m_edges.push_back(ar);
            }
            else
            {
                ar=new Edge(aa,bb,x,dir);
                aa->m_edges.push_back(ar);
                ar=new Edge(bb,aa,x,dir);
                bb->m_edges.push_back(ar);
            }
        }
    }
    bool search(N x,int& pos)
    {
        for (int i=0;i<m_nodes.size();i++){
            if (m_nodes[i]->m_data==x){
                pos=i;
                return 1;
            }
        }
        return 0;
    }
    bool delete_edge(Edge* x){
        x->m_nodes[0]->m_edges.remove(x);
        x->m_nodes[1]->m_edges.remove(x);
    }
    bool delete_Node(N x){
        int pos;
        int tam;
        if(!search(x,pos))
            return 0;
        else{
            tam=m_nodes[pos]->m_edges.size();
            for (int i=0;i<tam;i++){
                if (m_nodes[pos]->m_edges[i]->m_dir)
                    delete_edge(m_nodes[pos],m_nodes[pos]->m_edges[i]->m_data);
                else
                    delete_edge(m_nodes[pos],m_nodes[pos]->m_edges[i]->m_nodes[1],m_nodes[pos]->m_edges[i]->m_data);
            }
            m_nodes.erase(m_nodes.begin()+pos);
        }
    }
    bool find_edge(Node* a, E x, int& q, Edge ** p){
        for(int j=0;j<a->m_edges.size();j++)
        {
            if (a->m_edges[1]->m_data==x)
            {
                q=j;
                p=&a->m_edges[j];
                return 1;
            }
        }
        return 0;
    }
    /// Encontrar si 2 nodos se conectan.
    bool find_edge_x(Node* a, Node* b)
    {
        for(int j=0;j<a->m_edges.size();j++)
        {
            if (a->m_edges[j]->m_nodes[0]==b or a->m_edges[j]->m_nodes[1]==b )
            {
//                q=j;
//                p=&a->m_edges[j];
                return 1;
            }
        }
        return 0;
    }

    bool delete_edge(Node* a, Node *b, E x)
    {
        int q;
        Edge** p;
        if (!find_edge(a,x,q,p)) return 0;
        a->m_edges.erase(a->m_edges.begin()+q);
        if (!find_edge(b,x,q,p)) return 0;
        b->m_edges.erase(b->m_edges.begin()+q);
        return 1;
    }

    bool delete_edge(Node* a, E x){
        int q;
        Edge** p;
        if (!find_edge(a,x,q,p)) return 0;
        a->m_edges.erase(a->m_edges.begin()+q);
        delete *p;
    }
    void print()
    {
        for (int i=0;i<m_nodes.size();i++)
        {
            cout<<m_nodes[i]->m_data<<" -> ";
            for(int j=0;j<m_nodes[i]->m_edges.size();j++)
            {
                if (m_nodes[i]->m_edges[j]!=NULL)
                {
                    cout<<m_nodes[i]->m_edges[j]->m_nodes[1]->m_data<<" ; ";
                }
                //cout<<m_nodes[i]->m_edges[j]->m_data<<" ; ";
            }
            cout<<endl;
        }
    }


    Edge* E_ran()
    {
        Node* x;
        Node* y;
        Edge* b;
        int t_nod,t_ver,nod_num,nod_ver;
        t_nod = m_nodes.size();
//        srand(time(NULL));
        nod_num = rand()%t_nod;
        //cout <<"nodo randon: "<< nod_num<< endl;
        t_ver = m_nodes[nod_num]->m_edges.size();
        nod_ver = rand()%t_ver;
        //cout<<"arista randon: "<< nod_ver<< endl;
        b = m_nodes[nod_num]->m_edges[nod_ver];
        //cout<<nod_num<<endl;
        return b;
    }
    void delete_bucles()
    {
        Node* a;
        for (int i=0;i<m_nodes.size();i++)
        {
            for(int j=0;j<m_nodes[i]->m_edges.size();j++)
            {
                if (m_nodes[i]->m_edges[j]!=NULL)
                {
                    if((m_nodes[i]->m_edges[j]->m_nodes[0]->m_data == m_nodes[i]->m_data) and (m_nodes[i]->m_edges[j]->m_nodes[1]->m_data == m_nodes[i]->m_data))
                    {
                        a = m_nodes[i];
                        cout<<"tiene bucle en: "<<a -> m_data<<endl;
                        delete_edge_y(a,j);
                    }
                }
            }
        }
    }
    bool delete_edge_y(Node* a, int j)
    {
        int q;
        Edge* p = a->m_edges[j];
        for(int i=0;i<p->m_nodes[1]->m_edges.size();i++)
        {
            if(p->m_nodes[1]->m_edges[i]->m_nodes[1]==a)
            {
                p->m_nodes[1]->m_edges.erase(p->m_nodes[1]->m_edges.begin()+i);
                p->m_nodes[0]->m_edges.erase(p->m_nodes[0]->m_edges.begin()+j);
                delete p;
                return 1;
            }
        }

    }

    void actualizar_nodo(Node* c,Node* a,Node* b)
    {
//        cout<<"actualizando Nodos "<< c->m_data<< " con " <<a->m_data<<endl;
        for(int i=0;i<c->m_edges.size();i++)
        {
            if(c->m_edges[i]->m_nodes[1]==b)
            {
                c->m_edges[i]->m_nodes[1]=a;
                break;
            }
        }
    }
    void unir_nodos (Node* a, Node* b,Edge* x)
    {
        /// 1) Eliminar todas las aristas que se apuntan entre los 2 nodos
        Node* aa;
        Edge* ar;
        //cout<<"tam :" <<a->m_edges.size()<<endl;
        for(int j=0;j<a->m_edges.size();j++)
        {
            if(a->m_edges[j]->m_nodes[1]==b)
            {
//                cout<<"Eliminando arista"<<endl;
                delete_edge_y(a,j);
                j--;
            }
        }
//        for(int j=0;j<b->m_edges.size();j++)
//        {
//            if(b->m_edges[j]->m_nodes[1]==a)
//            {
////                cout<<"Eliminando arista"<<endl;
//                delete_edge_y(b,j);
//                j--;
//            }
//        }
        //cout<< "1) Eliminar todas las aristas que se apuntan entre los 2 nodos"<<endl;
//        print();

        /// 2) Elegir el que tiene mas aristas
        /// 3) Apuntar todos los nodos de B a A

        Node* c;
        int i=0;
        if(a->m_edges.size()>=b->m_edges.size())
        {
//            cout<< "1) IF "<<endl;
//            cout<< a->m_edges.size()<< " >= " <<b->m_edges.size()<<endl;
//            cout<<"Nuevo nodo: "<<a->m_data<<endl;
            while (i!=b->m_edges.size())
            {
                b->m_edges[i]->m_nodes[0]=a;
                c=b->m_edges[i]->m_nodes[1];
                ar=new Edge(a,c,0,0);
                a->m_edges.push_back(ar);
                actualizar_nodo(c,a,b);
                b->m_edges.erase(b->m_edges.begin()+i);
            }
//            c=b->m_edges[0]->m_nodes[1];
//            ar=new Edge(a,c,0,0);
//            a->m_edges.push_back(ar);
//            actualizar_nodo(c,a,b);
//            b->m_edges.erase(b->m_edges.begin());
            for (int i=0;i<m_nodes.size();i++)
            {
                if(m_nodes[i]==b) m_nodes.erase(m_nodes.begin()+i);
            }
        }
        else if(a->m_edges.size()<b->m_edges.size())
        {
//            cout<< "2) IF "<<endl;
//            //cout<< a->m_edges.size()<< " >= " <<b->m_edges.size()<<endl;
//            cout<<"Nuevo nodo: "<<a->m_data<<endl;
            while(i!=a->m_edges.size())
            {
                a->m_edges[0]->m_nodes[0]=b;
                c=a->m_edges[0]->m_nodes[1];
                ar=new Edge(b,c,0,0);
                b->m_edges.push_back(ar);
                actualizar_nodo(c,b,a);
                a->m_edges.erase(a->m_edges.begin()+i);
            }
//            c=a->m_edges[0]->m_nodes[1];
//            ar=new Edge(b,c,0,0);
//            b->m_edges.push_back(ar);
//            actualizar_nodo(c,b,a);
//            a->m_edges.erase(a->m_edges.begin());

            for (int i=0;i<m_nodes.size();i++)
            {
                if(m_nodes[i]==a) m_nodes.erase(m_nodes.begin()+i);
            }

        }
        //cout<< "2-3-4) Elegir el que tiene mas aristas ,Apuntar todos los nodos de B a A y Eliminar el nodo B"<<endl;
            /// 4) Eliminar el nodo B
//        print();

    }
    int max_flow()
    {
        while(m_nodes.size()>2)
        {
            Edge* b = E_ran();
            delete_bucles();
//            cout << "Nodos a unir "<< b->m_nodes[0]->m_data<< " , "<< b->m_nodes[1]->m_data<<endl;
            unir_nodos(b->m_nodes[0],b->m_nodes[1],b);
//            cout<<endl;
        }
        delete_bucles();
//        cout<< m_nodes[0]->m_edges.size()<<endl;
        return m_nodes[0]->m_edges.size();
        //cout<< m_nodes[1]->m_edges.size()<<endl;
//        print();

    }
};




#endif // GRAPH_H_INCLUDED
