#include <iostream>
#include "graph.h"
#include <fstream>
#include <string>
#include <sstream>
#include <stdlib.h>

using namespace std;


void parceo (CGraph<int,int> &grafo)
{
    ///Agregamos LOS NODOS///
    ///Cambiar a grafo1.txt para probar con el de 8 nodos///
    ///Cambiar a grafo2.txt para probar con el de 200 nodos///

    ifstream nodos("grafo2.txt");
    int i,j;
    char c;
    string cadena,low;
    c = nodos.get();
    i = c - '0';
    //cout<<c<<"nodo"<<endl;
    grafo.insertar_Node(i);
    while(!nodos.eof())
    {
        c = nodos.get();
        if (c=='\n')
        {
            c = nodos.get();
            while(c!=' ')
            {
                //i = c - '0';
                cadena = cadena + c;
                c = nodos.get();
            }
            //cout<<cadena<<" nodo"<<endl;
            i = atoi(cadena.c_str());
            grafo.insertar_Node(i);
            cadena ="";
        }
    }
    nodos.close();

    ///Agregamos LAS ARISTAS///
    ///Cambiar a grafo1.txt para probar con el de 8 nodos///
    ///Cambiar a grafo2.txt para probar con el de 200 nodos///
    cadena = "";
    ifstream ve("grafo2.txt");
    c = ve.get();
    i = c - '0';
    j = i;
    //cout<<"Nodo: "<<j<< " ";
    c = ve.get();
    ve >> cadena;
    i = atoi(cadena.c_str());
    //cout << i<< " ";
    grafo.insertar_arista_x(j,i,0,0);
    cadena= "";
    while(!ve.eof())
    {
        c = ve.get();
        if (c ==' ')
        {
            ve >> cadena;
            i = atoi(cadena.c_str());
            //cout << i<< " ";
            grafo.insertar_arista_x(j,i,0,0);
        }
        else if (c == '\n')
        {
            //cout<<endl;
            ve >> cadena;
            j = atoi(cadena.c_str());
            //cout <<"nodo :"<< j<< " ";
        }
    }
    ve.close();
    //cout<<endl;
}
int max_flow()
{
    CGraph<int,int> grafo;
    parceo(grafo);
    return grafo.max_flow();
}

int main()
{

    srand((rdtsc()));
    CGraph<int,int> grafo;
    parceo(grafo);

//    grafo.insertar_Node(1);
//    grafo.insertar_Node(2);
//    grafo.insertar_Node(3);
//    grafo.insertar_Node(4);
//    grafo.insertar_Node(5);
//    grafo.insertar_arista(grafo.m_nodes[0],grafo.m_nodes[1],4,0);
//    grafo.insertar_arista(grafo.m_nodes[1],grafo.m_nodes[2],4,0);
//    grafo.insertar_arista(grafo.m_nodes[3],grafo.m_nodes[1],4,0);
//    grafo.insertar_arista(grafo.m_nodes[4],grafo.m_nodes[1],4,0);
//    grafo.insertar_arista(grafo.m_nodes[0],grafo.m_nodes[1],4,0);
//    grafo.print();
//    cout<<"-----------------------"<<endl;
//    grafo.delete_Node(1);
    //grafo.print();

    grafo.max_flow();
    //grafo.print();
    int j=200,k;

    for(int i=0;i<50;i++)
    {
        k=grafo.max_flow();
        if(k<j) j=k;
    }
//system("cls");
    cout << "Corte minimo: "<<k<<endl;

}
